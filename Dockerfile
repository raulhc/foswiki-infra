FROM perl:5.14-slim-buster

RUN apt-get update && \
  apt-get install -y \
  antiword apache2 bash ca-certificates db-util gcc graphviz grep imagemagick \
  logwatch lynx make mime-support musl musl-dev nano odt2txt openssl perl perl-base \
  perl-modules perlmagick poppler-utils rcs tzdata unzip wget zip \
  libalgorithm-diff-perl libalgorithm-diff-xs-perl libalgorithm-merge-perl \
  libapache2-mod-authz-unixgroup libapache2-mod-perl2 libapache2-reload-perl \
  libarchive-zip-perl libbsd-resource-perl \
  libcgi-session-perl libclass-isa-perl libclass-mix-perl libcommon-sense-perl \
  libcrypt-eksblowfish-perl libcrypt-passwdmd5-perl libdata-optlist-perl \
  libdate-manip-perl libdbd-mysql-perl libdbi-perl libdevel-symdump-perl \
  libdigest-hmac-perl libdpkg-perl libencode-locale-perl liberror-perl libesedb-utils \
  libfile-copy-recursive-perl libfile-fcntllock-perl libfile-listing-perl libfont-afm-perl \
  libgetopt-long-descriptive-perl libhtml-form-perl libhtml-format-perl libhtml-parser-perl \
  libhtml-tagset-perl libhtml-template-perl libhtml-tree-perl libhttp-cookies-perl \
  libhttp-daemon-perl libhttp-date-perl libhttp-message-perl libhttp-negotiate-perl \
  libimage-magick-perl libio-socket-ip-perl libio-socket-ssl-perl libio-stringy-perl \
  libipc-signal-perl libjson-perl libjson-xs-perl liblocale-gettext-perl \
  liblocale-maketext-lexicon-perl liblwp-mediatypes-perl liblwp-protocol-https-perl \
  libmailtools-perl libmime-types-perl libmodule-implementation-perl libmodule-runtime-perl \
  libnet-dns-perl libnet-http-perl libnet-ip-perl libnet-ssleay-perl libparams-classify-perl \
  libparams-util-perl libparams-validate-perl libpcre3:amd64 libposix-strptime-perl \
  libproc-waitstat-perl libsocket-perl libsub-exporter-perl libsub-install-perl \
  libswitch-perl libtext-charwidth-perl libtext-diff-perl libtext-iconv-perl \
  libtext-unidecode-perl libtext-wrapi18n-perl libtimedate-perl libtry-tiny-perl liburi-perl \
  libuuid-perl libwww-perl libwww-robotrules-perl libyaml-syck-perl

COPY foswiki-apache.conf /etc/apache2/sites-available/foswiki-apache.conf
RUN mkdir -p /var/lib/foswiki
RUN a2dissite 000-default
RUN a2ensite foswiki-apache
RUN a2enmod authz_unixgroup
RUN a2enmod cgid
RUN a2enmod include
RUN a2enmod rewrite
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
