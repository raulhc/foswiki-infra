cnf ?= .env
include $(cnf)
export $(shell sed 's/=.*//' $(cnf))

# Get the latest tag
TAG=$(shell git describe --tags --abbrev=0)
GIT_COMMIT=$(shell git log -1 --format=%h)

DOCKER_CMD=docker run -it -e CLOUDFLARE_API_TOKEN=${CLOUDFLARE_API_TOKEN}  -v $$HOME/.ssh/:/root/.ssh/ -v $$PWD:/code -w /code --entrypoint "" hashicorp/terraform

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help plan init clean apply

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help


# DOCKER TASKS

init: ## Create the terraform environment
	$(DOCKER_CMD) terraform init

clean: ## Create the terraform environment
	rm -fr .terraform*
	rm terraform.tfstate*

plan: ## Create the terraform's plan
	$(DOCKER_CMD) terraform plan -out plano

apply: ## Apply the plan
	$(DOCKER_CMD) terraform apply "plano"
	rm -fr plano

destroy: ## Apply the plan
	$(DOCKER_CMD) terraform destroy -auto-approve
	rm -fr plano

bash: ## Terminal for mac users
	$(DOCKER_CMD_MAC) bash