module "cloudflare_record_fossil_ipv4" {
  source  = "git@github.com:mentoriaiac/iac-modulo-cloudflare.git?ref=v0.1"
  zone_id = "dcafaad9eb248666898b58e082362a07"
  name    = "fossil"
  value   = "167.172.226.201"
  type    = "A"
  proxied = true
  ttl     = 1
}